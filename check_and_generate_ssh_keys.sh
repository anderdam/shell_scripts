#!/bin/bash

# Define the SSH directory path
ssh_dir="$HOME/.ssh"

# Check if the SSH directory exists and contains key files
if [[ -d "$ssh_dir" && "$(ls -A "$ssh_dir" | grep -E '\.pub$')" ]]; then
  echo "SSH directory exists and contains key files."
  ls -A "$ssh_dir"
  echo " "
  echo "Use cat $HOME/.ssh/<file name> for private key."
  echo "Use cat $HOME/.ssh/<file name>.pub for public key." 
else
  # Directory doesn't exist or is empty
  read -p "SSH directory doesn't exist or is empty. Create it with keys? (Y/n): " answer
  answer=${answer:-Y}  # Set default answer to "Y" if user doesn't enter anything

  if [[ "$answer" =~ ^[Yy]$ ]]; then
    # User wants to create the directory and keys (or didn't provide input)
    mkdir -p "$ssh_dir"  # Create the directory with parent directories if needed
    
    # Define key algorithm
    echo ""
    echo "Choose key format:
    ed25519 (preferred / default option)
    ed25519_sk
    ecdsa
    ecdsa_sk
    rsa    
    dsa (*** deprecated - not recomended ***)"
    
    echo ""
    read -p "Enter your option (lower case):" algorithm
    algorithm=${algorithm:-ed25519}
  
    #Generate key
    read -p "Insert a key name: " coment   
  
    ssh-keygen -t $algorithm -b 4096 -C $coment
    echo " "
    echo "SSH directory created and keys generated successfully."
    
    # Show options to print keys
    echo ""
    echo "Private key ->  cat $HOME/.ssh/id_${algorithm}"
    echo "Public key ->  cat $HOME/.ssh/id_${algorithm}.pub" 
  else
    # User explicitly chose "n"
    echo "Okay, exiting without creating the SSH directory or keys."
  fi
fi
