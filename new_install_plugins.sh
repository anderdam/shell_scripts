#!/usr/bin/env sh

plugins_list=(git archlinux autoenv cp direnv dirhistory docker-compose docker dotenv fastfile fzf aws azure gcloud gitignore history jsontools keychain nmap pip pre-commit pyenv pylint python qrcode rsync rtx safe-paste snap wd web-search z)


# Function to clone a repository if it doesn't already exist
clone_repo() {
    local repo_url=$1
    local repo_name=$(basename $repo_url .git)
    local repo_dir=$HOME/.oh-my-zsh/custom/plugins/$repo_name

    if [[ ! -d $repo_dir ]]; then
        git clone --depth 1 $repo_url $repo_dir
        echo "Cloned $repo_name successfully!"
    fi

    plugins_list+=($repo_name)
}

# Clone the specified repositories
clone_repo "https://github.com/zsh-users/zsh-syntax-highlighting.git"
clone_repo "https://github.com/marlonrichert/zsh-autocomplete.git"
clone_repo "https://github.com/zsh-users/zsh-history-substring-search.git"
clone_repo "https://github.com/MichaelAquilina/zsh-you-should-use.git"
clone_repo "https://github.com/fdellwing/zsh-bat.git"

# Prompt the user for additional plugins
echo "Plugins to be installed: ${plugins_list[@]}"
#read -p "Do you want to add any other plugins? (y/n) " add_more
#
#if [[ $add_more =~ ^[Yy]$ ]]; then
#    read -p "Enter the names of the plugins you want to add, separated by spaces: " user_plugins
#    IFS= ' ' read -r -a user_plugins_array <<< "$user_plugins"
#
#    for plugin in "${user_plugins_array[@]}" do
#        repo_dir=$HOME/.oh-my-zsh/plugins/$plugin
#
#        if [[ -d $repo_dir ]]; then
#            echo "Plugin $plugin already exists, adding it to the list."
#            plugins_list+=($plugin)
#        else
#            read -p "Enter the full Git URL for $plugin: " user_repo_url
#            clone_repo $user_repo_url
#        fi
#    done
#fi

# Update the plugins list in ~/.zshrc using awk
cp ~/.zshrc ~/.zshrc_backup

awk -v plugins_list="$plugins_list" '/^plugins=(git)/{ print "plugins=(" plugins_list ")" }' ~/.zshrc > ~/.zshrc.tmp && mv ~/.zshrc.tmp ~/.zshrc


source ~/.zshrc

echo "Plugins installed and ~/.zshrc updated successfully!"
